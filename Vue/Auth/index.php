<?php $this->titre = "Mon Blog - Connexion" ?>


<form action="<?= $this->lien('auth/login'); ?>" method="post">
<div class="row uniform">
	<div class="6u 12u$(xsmall)">
	    <input name="login" type="text" placeholder="Entrez votre login" required autofocus>
	</div>
	<div class="6u 12u$(xsmall)">
  	  <input name="mdp" type="password" placeholder="Entrez votre mot de passe" required>
  	</div>
  	<div class="actions">
 	   <input type="submit" value="Se connecter" name="envoi" />
 	 </div>
</form>