<?php

require_once 'Framework/Controleur.php';
require_once 'Modele/Utilisateur.php';

class ControleurUser extends Controleur {

    private $user;

    public function __construct() {
        $this->user = new Utilisateur();
    }

    // Affiche la liste de tous les billets du blog
    public function index() {
    	if(!$this->requete->getSession()->existeAttribut('idUtilisateur'))
    		throw new Exception("Page inacessible");
    	if(!$this->requete->existeParametre('id'))
    		throw new Exception("Pas d'utilisateur trouvé");
        $util = $this->user->getUtilisateurById($this->requete->getParametre('id'));

        $this->genererVue(array('util' => $util, 'ami' => $this->user->estAmi($this->requete->getSession()->getAttribut('idUtilisateur'), $util['UTIL_ID'])));
    }

    public function friend(){
		if(!$this->requete->getSession()->existeAttribut('idUtilisateur'))
    		throw new Exception("Page inacessible");
    	if(!$this->requete->existeParametre('id'))
    		throw new Exception("Pas d'utilisateur trouvé"); 

    	$util = $this->user->getUtilisateurById($this->requete->getParametre('id'));
    	if($this->user->estAmi($this->requete->getSession()->getAttribut('idUtilisateur'), $util['UTIL_ID']))
    		throw new Exception("Ami déjà ajouté");
    	else
   			$this->user->ajouterAmi($this->requete->getSession()->getAttribut('idUtilisateur'), $util['UTIL_ID']);
   		$this->alerte('success', 'Vous êtes maintenant amis !');
   		$this->rediriger('user', 'index/'.$util['UTIL_ID']);
    }

}

