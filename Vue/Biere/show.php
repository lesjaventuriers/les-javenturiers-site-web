<?php $this->titre = "Bière ".$biere->nom; ?>
        <!-- Presentation biere -->
	    <div class="6u 12u$(medium)">
	    	<header>
	    		<?="<h2>".$biere->nom."</h2>";?>
	    		<div id="6u 12u$(medium)">
	    			<div id="gauche">
						<?= "<img src=\"".$biere->photo."\" alt='image biere'>" ?>
					</div>
			 		<div id="droite">
                		<?= "<p>Type de bière : ".$biere->type."<br/>Degrés : ".$biere->degres."<br/> Brasseur : ".$biere->brasseur."<br />Moyenne : ".$moy."/10</p>"; ?>
                	</div>
            </header> 
        </div>


        <!-- Son avis -->
            <section id="main" class="wrapper">
	        <div class="inner-clear">
		    <h2 id="content">Donnez votre avis !</h2>
			<form method="post" action="#">
			    <div class="4u 12u$(xsmall)">
		            	<p>Note : </p>
			    </div>
			    <div class="8u 12u$(xsmall)">
			    	<input type="number" name="note" id="note" step="1" min="0" max="10"/>
			    </div>
			    <div class="4u 12u$(xsmall)">
			    	<p>Commentaires : </p>
			    </div>
			    <div class="8u 12u$(xsmall)">
			    	<textarea name="message" id="message" placeholder="Entrez votre commentaire" rows="6"></textarea>
			    <div class="8u 12u$(xsmall)">
			    <ul class="actions">
				<li><input type="submit" value="Envoyer votre avis" /></li>
				<li><input type="reset" value="Réinitialiser" class="alt" /></li>
			    </ul>
                        </article>
                    </div>
                </div>
                

            </section>
        



