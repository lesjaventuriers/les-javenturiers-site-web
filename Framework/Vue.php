<?php

require_once 'Configuration.php';


class Vue {

    
    private $fichier;

    
    private $titre;

    
    public function __construct($action, $controleur = "") {
        // Détermination du nom du fichier vue à partir de l'action et du constructeur
        // La convention de nommage des fichiers vues est : Vue/<$controleur>/<$action>.php
        $fichier = "Vue/";
        if ($controleur != "") {
            $fichier = $fichier . $controleur . "/";
        }
        $this->fichier = $fichier . $action . ".php";
    }

    
    public function generer($donnees) {
        // Génération de la partie spécifique de la vue
        $contenu = $this->genererFichier($this->fichier, $donnees);
        // On définit une variable locale accessible par la vue pour la racine Web
        // Il s'agit du chemin vers le site sur le serveur Web
        // Nécessaire pour les URI de type controleur/action/id
        $racineWeb = Configuration::get("racineWeb", "/");
        $data = $donnees;
        $data['titre'] = $this->titre;
        $data['racineWeb'] = $racineWeb;
        $data['contenu'] = $contenu;
        // Génération du gabarit commun utilisant la partie spécifique
        $vue = $this->genererFichier('Vue/gabarit.php',$data);
        // Renvoi de la vue générée au navigateur
        echo $vue;
    }

    
    private function genererFichier($fichier, $donnees) {
        if (file_exists($fichier)) {
            // Rend les éléments du tableau $donnees accessibles dans la vue
            extract($donnees);
            // Démarrage de la temporisation de sortie
            ob_start();
            // Inclut le fichier vue
            // Son résultat est placé dans le tampon de sortie
            require $fichier;
            // Arrêt de la temporisation et renvoi du tampon de sortie
            return ob_get_clean();
        }
        else {
            throw new Exception("Fichier '$fichier' introuvable");
        }
    }

    
    private function nettoyer($valeur) {
        return nl2br(htmlspecialchars($valeur, ENT_QUOTES, 'UTF-8', false));
    }


    private function lien($url)
    {
        return (substr($url, 0, 1) == '/') ? Configuration::get('racineWeb').substr($url, 1, strlen($url)) : Configuration::get('racineWeb').$url;
    }

    private function date($date, $format = "d/m/Y")
    {
        return date($format, strtotime($date));
    }

}
