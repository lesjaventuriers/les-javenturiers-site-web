<?php $this->titre = "Accueil"; ?>
        <!-- Banner -->
            <section id="banner">
                <h1>Beer-to-Beer</h1>
                <p>Apportez votre bière à l'édifice !</p>
            </section>

        <!-- One -->
            <section id="one" class="wrapper">
                <div class="inner">
                    <div class="flex flex-3">
                        <article>
                            <header>
                                <h3>Un réseau social et un site participatif<br /> Enjoy Beer-To-Beer !</h3>
                            </header>
                            <p>Conservez vos impressions sur ce que vous goûtez en soirée, mais surtout, organisez-en de nouvelles rapidement! <br/>(Et invitez nous!)</p>
                            <footer>
                                <!--<a href="#" class="button special">More</a>-->
                            </footer>
                        </article>
                        <article>
                            <header>
                                <h3>La bière: une passion!<br />Du brassage à la consommation.</h3>
                            </header>
                            <p>Beer-To-Beer accueille de nombreux consommateurs, évidemment! Mais nous comptons aussi de nombreux brasseurs indépendants parmi nos membres! Aidons-les à faire vivre ce patrimoine unique!</p>
                            <footer>
                                <!--<a href="#" class="button special">More</a>-->
                            </footer>
                        </article>
                        <article>
                            <header>
                                <h3>Venez avec vos amis<br />Une bonne bière se partage!</h3>
                            </header>
                            <p>La vocation sociale de notre site est indéniable. Rencontrez des amateurs de breuvages variés près de chez vous. Ils se feront un plaisir de vous offrir un verre (à charge de revanche évidemment)!<br/>Propager une bonne ambiance dans tous les bars et toutes les soirées est l'objectif premier de Beer-To-Beer!</p>
                            <footer>
                                <!--<a href="#" class="button special">More</a>-->
                            </footer>
                        </article>
                    </div>
                </div>
            </section>

        <!-- Two -->
            <section id="two" class="wrapper style1 special">
                <div class="inner">
                    <header>
                        <h2>Les Petits Nouveaux ;)</h2>
                        <p><em>"Sharing beer with a fascinating stranger is one of life's true delights"</em></p>
                    </header>
                    <div class="flex flex-4">
                        <div class="box person">
                            <div class="image round">
                                <img src="<?= $this->lien('Contenu/images/pic03.jpg'); ?>" alt="Person 1" />
                            </div>
                            <h3>JeremyD</h3>
                            <p>"Ah? M****!"</p>
                        </div>
                        <div class="box person">
                            <div class="image round">
                                <img src="<?= $this->lien('Contenu/images/pic04.jpg'); ?>" alt="Person 2" />
                            </div>
                            <h3>Lulu24</h3>
                            <p>"J'aime le thé aussi!"</p>
                        </div>
                        <div class="box person">
                            <div class="image round">
                                <img src="<?= $this->lien('Contenu/images/pic05.jpg'); ?>" alt="Person 3" />
                            </div>
                            <h3>FloTouriste</h3>
                            <p>"Blonde, brune, pourquoi choisir?"</p>
                        </div>
                        <div class="box person">
                            <div class="image round">
                                <img src="<?= $this->lien('Contenu/images/pic06.jpg'); ?>" alt="Person 4" />
                            </div>
                            <h3>Dolores974</h3>
                            <p>"Coma éthilique"</p>
                        </div>
                    </div>
                </div>
            </section>

        <!-- Three -->
            <section id="three" class="wrapper special">
                <div class="inner">
                    <header class="align-center">
                        <h2>La bière et vous<br/><em>C'est vous qui en parlez le mieux!</em></h2>
                        <p>Aidez-nous à enrichir notre site : partagez vos expériences sociales!</p>
                    </header>
                    <div class="flex flex-2">
                        <article>
                            <div class="image fit">
                                <img src="<?= $this->lien('Contenu/images/pic01.jpg'); ?>" alt="Pic 01" />
                            </div>
                            <header>
                                <h3>La bière au USA: Une voie sans issue?</h3>
                            </header>
                            <p>Eh, oui! Notre site compte quelques adeptes de journalisme. Découvrez leurs infos sur le brassage et l'importation de bière aux States! Pourrez-vous déguster une bonne pinte de votre produit préféré lorsque vous y serez? Rien n'est moins sûr...</p>
                            <footer>
                                <!--<a href="#" class="button special">More</a>-->
                            </footer>
                        </article>
                        <article>
                            <div class="image fit">
                                <img src="<?= $this->lien('Contenu/images/pic02.jpg'); ?>" alt="Pic 02" />
                            </div>
                            <header>
                                <h3>Concours photos de soirée!</h3>
                            </header>
                            <p>Contactez nous pour découvrir les soirées organisées près de chez vous par Beer-To-Beer! De grandes expériences gustatives et amicales en perspective.</p>
                            <footer>
                                <!--<a href="#" class="button special">More</a>-->
                            </footer>
                        </article>
                    </div>
                </div>
            </section>
