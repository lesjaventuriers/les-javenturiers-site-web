<?php
require_once 'Framework/Modele.php';

class Biere extends Modele {

	public function __construct() {
	} 

	public function insertBiere($nom, $type, $degre, $brasseur, $photo) {
		$sql = "INSERT INTO 24h_biere (nom, type, degres, brasseur, photo) VALUES (?,?,?,?,?)";
        $this->executerRequete($sql, array($nom, $type, $degre, $brasseur, $photo));
        return true ;
	}

	public function getId($nom) {
		$sql = "SELECT id FROM 24h_biere WHERE nom=?" ;
		$res = $this->executerRequete($sql, array($nom)) ;
		return $res->fetch(PDO::FETCH_OBJ) ;
	}

	public function existeDeja($nom) {
		$sql = "SELECT id FROM 24h_biere WHERE nom=?" ;
		$res = $this->executerRequete($sql, array($nom)) ;
		return ($res->rowCount()==1) ;
	}

	public function recuperer($id){
		$sql = "SELECT * FROM 24h_biere WHERE id=?";
		$res=$this->executerRequete($sql,array($id));
	return( $res->fetch(PDO::FETCH_OBJ));
	}

	public function rechercher($search)
	{
		$sql = "SELECT * FROM 24h_biere WHERE nom LIKE ?";
		$res = $this->executerRequete($sql, array("%".$search."%")) ;	
		return $res->fetchAll(PDO::FETCH_OBJ);
	}

	public function insererCom($n, $auteur, $biere, $com) {
		$sql = "INSERT INTO 24h_avis (note, auteur_id, biere_id, commentaire) VALUES (?,?,?,?)";
        $this->executerRequete($sql, array($n, $auteur, $biere, $com));
	}

	public function moyenne($id) {
		$sql = "Select Avg(note) moy from 24h_avis group by biere_id having biere_id=?" ;
		$res = $this->executerRequete($sql, array($id));
		return $res->fetch(PDO::FETCH_OBJ)->moy ;
	}

	public function afficherCom($id) {
		$sql = "Select commentaire, auteur_id, note from 24h_avis where biere_id=?" ;
		$this->executerRequete($sql, array($id));
		return $res->fetchAll(PDO::FETCH_OBJ) ;
	}
}