<?php $this->titre = "Voir un profil"; ?>

<div class="table-wrapper">
	<table>
		<thead>
			<tr>
			<th>Login</th>
			<th>Nom</th>
			<th>Prenom</th>
			<th>Mail</th>
			</tr>
		</thead>
		<tbody>
			<tr>
			<th>
				<?= $util['UTIL_LOGIN']; ?>
			</th>
			<th>
				<?= $util['UTIL_NOM']; ?>
			</th>
			<th>
				<?= $util['UTIL_PRENOM']; ?>
			</th>
			<th>
				<?= $util['UTIL_MAIL']; ?>
			</th>
			</tr>
		</tbody>
	</table>
	
	<img src="<?= $util['avatar']; ?>" alt="avatar" />
</div>

<?php if($util['UTIL_LOGIN'] == $session->getAttribut("login")){
	echo '<a href="'.$this->lien("auth/profile").'" class="button special small fit">Editer mon profil</a>';
}else{
	if(!$ami)
		echo '<a class="button special small fit" href="'.$this->lien("user/friend/".$util['UTIL_ID']).'">Ajouter en ami</a>';
	else
		echo '<a class="button special small fit" href="#">'.$util['UTIL_LOGIN'].' est votre ami.</a>';

}