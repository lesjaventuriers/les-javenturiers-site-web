<?php $this->titre = "Inscription"; ?>
<script src='https://www.google.com/recaptcha/api.js'></script>

<form action="#" method="POST">
<div class="row uniform">
	<div class="4u 12u$(xsmall)">
		<label for="pseudo">Pseudo :</label>
	</div>
	<div class="8u 12u$(xsmall)">
		<input type="text" name="pseudo" id="pseudo" />
	</div>

	<div class="4u 12u$(xsmall)">
		<label for="pseudo">Nom :</label>
	</div>
	<div class="4u 12u$(xsmall)">
		<input type="text" name="nom" id="nom" placeholder="Nom (facultatif)" />
	</div>
	<div class="4u 12u$(xsmall)">
		<input type="text" name="prenom" id="prenom" placeholder="Prénom (facultatif)" />
	</div>
	
	<div class="4u 12u$(xsmall)">
		<label for="mail">E-mail :</label>
	</div>

	<div class="8u 12u$(xsmall)">
		<input type="email" name="mail" id="mail" />
	</div>

	<div class="4u 12u$(xsmall)">
		<label for="password">Mot de passe :</label>
	</div>
	<div class="8u 12u$(xsmall)">
		<input type="password" name="password" id="password" />
	</div>
	<div class="4u 12u$(xsmall)">
		<label for="confirm">Confirmez votre mot de passe :</label>
	</div>
	<div class="8u 12u$(xsmall)">
		<input type="password" name="confirm" id="confirm" />
	</div>
	<div class="4u 12u$(xsmall)">
		<label>Prouvez que vous n'êtes pas un robot...</label>
	</div>
	<div class="8u 12u(xsmall)">
		<div class="g-recaptcha" data-sitekey="6LecMyIUAAAAAOV1zwqnqL5m33E4H9f6m6jdQ877"></div>
	</div>
	<div class="actions">
		<input type="submit" name="envoi" value="S'inscrire" />
	</div>
</div>
</form>