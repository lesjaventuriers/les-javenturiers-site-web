<?php

require_once 'Configuration.php';
require_once 'Requete.php';
require_once 'Vue.php';


abstract class Controleur {

    
    private $action;
    
    
    protected $requete;

    
    public function setRequete(Requete $requete)
    {
        $this->requete = $requete;
    }

    
    public function executerAction($action)
    {
        if (method_exists($this, $action)) {
            $this->action = $action;
            $this->{$this->action}();
        }
        else {
            $classeControleur = get_class($this);
            throw new Exception("Action '$action' non définie dans la classe $classeControleur");
        }
    }

    
    public abstract function index();

    
    protected function genererVue($donneesVue = array(), $action = null)
    {
        // Utilisation de l'action actuelle par défaut
        $actionVue = $this->action;
        if ($action != null) {
            // Utilisation de l'action passée en paramètre
            $actionVue = $action;
        }
        // Utilisation du nom du contrôleur actuel
         $classeControleur = get_class($this);
        $controleurVue = str_replace("Controleur", "", $classeControleur);

        // Instanciation et génération de la vue
        $vue = new Vue($actionVue, $controleurVue);
        $donneesVue['session'] = $this->requete->getSession();
        $vue->generer($donneesVue);
     }

    
    protected function rediriger($controleur, $action = null)
    {
        $racineWeb = Configuration::get("racineWeb", "/");
        // Redirection vers l'URL /racine_site/controleur/action
        header("Location:" . $racineWeb . $controleur . "/" . $action);
    }

    
    protected function alerte($type, $msg)
    {
        if(strtolower($type) != "error" && strtolower($type) != "warning" && strtolower($type) != "success" && strtolower($type) != "info")
            throw new Error("L'alerte '$type' n'existe pas !");
        $this->requete->getSession()->setAttribut("msgalertsys".strtolower($type), $msg);
    }
}
