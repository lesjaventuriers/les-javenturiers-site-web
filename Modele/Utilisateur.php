<?php
require_once 'Framework/Modele.php';

class Utilisateur extends Modele{
    public function connecter($login, $mdp)
    {
        $sql = "select UTIL_ID from 24h_utilisateur where UTIL_LOGIN=? and UTIL_MDP=?";
        $utilisateur = $this->executerRequete($sql, array($login, $mdp));
        return ($utilisateur->rowCount() == 1);
    }
    public function getUtilisateur($login, $mdp = null)
    {
        if($mdp != null)
        {
          $sql = "select UTIL_ID as idUtilisateur, UTIL_LOGIN as login, UTIL_MDP as mdp, UTIL_MAIL as mail, UTIL_NOM as nom, UTIL_PRENOM as prenom, UTIL_AVATAR as avatar from 24h_utilisateur where UTIL_LOGIN=? and UTIL_MDP=?";
         $utilisateur = $this->executerRequete($sql, array($login, $mdp));    
        }else{
          $sql = "select UTIL_ID as idUtilisateur, UTIL_LOGIN as login, UTIL_MDP as mdp, UTIL_MAIL as mail, UTIL_NOM as nom, UTIL_PRENOM as prenom, UTIL_AVATAR as avatar from 24h_utilisateur where UTIL_LOGIN=?";
         $utilisateur = $this->executerRequete($sql, array($login));               
        }
        
        if ($utilisateur->rowCount() == 1)
            return $utilisateur->fetch();  // Accès à la première ligne de résultat
        else
            throw new Exception("Aucun utilisateur ne correspond aux identifiants fournis");
    }

    public function getUtilisateurById($id)
    {
        $sql = "select* from 24h_utilisateur where UTIL_ID=?";
         $utilisateur = $this->executerRequete($sql, array($id));        
        
        if ($utilisateur->rowCount() == 1)
            return $utilisateur->fetch();  // Accès à la première ligne de résultat
        else
            throw new Exception("Aucun utilisateur ne correspond aux identifiants fournis");
    }

    public function insertUtilisateur($login, $mail, $mdp, $prenom, $nom)
    {
        $sql = "Insert into 24h_utilisateur (UTIL_LOGIN, UTIL_MDP, UTIL_MAIL, UTIL_PRENOM, UTIL_NOM) VALUES (?,?,?,?,?)";
        $this->executerRequete($sql, array($login, $mdp, $mail, $prenom, $nom));
    }

    public function editUtilisateur($id, $mail, $prenom, $nom, $avatar, $mdp = null)
    {
        if($mdp != null)
        {
          $sql = "UPDATE 24h_utilisateur SET UTIL_MDP = ?, UTIL_MAIL = ?, UTIL_PRENOM = ?, UTIL_NOM = ?, UTIL_AVATAR = ? WHERE UTIL_ID = ?";
          $this->executerRequete($sql, array($mdp, $mail, $prenom, $nom, $avatar, $id));
        }
        else
        {
            $sql = "UPDATE 24h_utilisateur SET UTIL_MAIL = ?, UTIL_PRENOM = ?, UTIL_NOM = ?, UTIL_AVATAR = ? WHERE UTIL_ID = ?";
            $this->executerRequete($sql, array($mail, $prenom, $nom, $avatar, $id));
        }
    }

    public function pseudoUtilise($pseudo)
    {
        $sql = "SELECT * FROM 24h_utilisateur WHERE UTIL_LOGIN = ?";
        $resultat = $this->executerRequete($sql, array($pseudo));
        if($resultat->rowCount() ==0)
            return false;
        else
            return true;
    }

    public function mailUtilise($mail)
    {
        $sql = "SELECT * FROM 24h_utilisateur WHERE UTIL_MAIL = ?";
        $resultat = $this->executerRequete($sql, array($mail));
        if($resultat->rowCount() ==0)
            return false;
        else
            return true;
    }

    public function rechercher($search)
    {
        $sql = "SELECT * FROM 24h_utilisateur WHERE UTIL_LOGIN LIKE ? || UTIL_PRENOM LIKE ? || UTIL_NOM LIKE ?";
        $res = $this->executerRequete($sql, array("%".$search."%","%".$search."%","%".$search."%")) ;   
        return $res->fetchAll(PDO::FETCH_OBJ);
    }

    public function ajouterAmi($me, $id)
    {
        $sql = "INSERT INTO 24h_amis (utilisateur1, utilisateur2) VALUES (?,?)";
        $res = $this->executerRequete($sql, array($me, $id)) ;   
    }

    public function estAmi($me, $id)
    {
        $sql = "SELECT id FROM 24h_amis WHERE utilisateur1 = ? && utilisateur2 = ?";
        $res = $this->executerRequete($sql, array($me, $id)) ;   
        return ($res->rowCount()==1);        
    }
}
