<?php

require_once 'Framework/Controleur.php';
require_once 'Modele/Utilisateur.php';


class ControleurAuth extends Controleur
{
    private $utilisateur;

    public function __construct()
    {
        $this->utilisateur = new Utilisateur();
    }

    public function index()
    {
    	if($this->requete->getSession()->existeAttribut("idUtilisateur"))
    	{
    		$this->alerte('error', 'Vous êtes déjà connecté.');
    		$this->rediriger("accueil");
    	}
        $this->genererVue();
    }

    public function login()
    {
    	if($this->requete->getSession()->existeAttribut("idUtilisateur"))
    	{
    		$this->alerte('error', 'Vous êtes déjà connecté.');
    		$this->rediriger("accueil");
    	}
        if ($this->requete->existeParametre("login") && $this->requete->existeParametre("mdp")) {
            $login = $this->requete->getParametre("login");
            $mdp = $this->requete->getParametre("mdp");
            if ($this->utilisateur->connecter($login, sha1($mdp))) {
                $utilisateur = $this->utilisateur->getUtilisateur($login, sha1($mdp));
                $this->requete->getSession()->setAttribut("idUtilisateur",
                        $utilisateur['idUtilisateur']);
                $this->requete->getSession()->setAttribut("login",
                        $utilisateur['login']);
                $this->requete->getSession()->setAttribut("mail",
                        $utilisateur['mail']);
                $this->alerte('success', 'Bienvenue '.$utilisateur['login']);
                $this->rediriger("accueil");
            }
            else
            {
            	$this->alerte('warning', 'Erreur pseudo/mot de passe');
                $this->genererVue(array(),
                        "index");
            }
        }
        else
            throw new Exception("Action impossible : login ou mot de passe non défini");
    }

    public function logout()
    {
        $this->requete->getSession()->detruire();
        $this->rediriger("accueil");
    }

    public function register()
    {
         if($this->requete->getSession()->existeAttribut("idUtilisateur"))
        {
            $this->alerte('error', 'Vous êtes déjà connecté.');
            $this->rediriger("accueil");
        }

        if($this->requete->existeParametre("envoi"))
        {
            if(!$this->requete->existeParametre("pseudo"))
            {
                $this->alerte('warning', 'Le champ "pseudo" est obligatoire.'); 
                $this->genererVue();         
            }
            elseif(!$this->requete->existeParametre("password"))
            {
                 $this->alerte('warning', 'Le champ "Mot de passe" est obligatoire.');     
                $this->genererVue();

            }
            elseif(!$this->requete->existeParametre("confirm"))
            {
                 $this->alerte('warning', 'Le champ "Confirmez le mot de passe" est obligatoire.');
                $this->genererVue();

            }
            elseif(!$this->requete->existeParametre("mail"))
            {
                 $this->alerte('warning', 'Le champ "Mail" est obligatoire.');  
                $this->genererVue();

            }
            elseif(strlen($this->requete->getParametre("pseudo")) < 5)
            {
                 $this->alerte('warning', 'Votre pseudo est trop court.');    
                $this->genererVue();                   
            }
            elseif(strlen($this->requete->getParametre("password")) < 5)
            {
                 $this->alerte('warning', 'Votre mot de passe est trop court.');    
                $this->genererVue();                   
            }
            elseif(strcmp($this->requete->getParametre("password"),$this->requete->getParametre("confirm"))!=0)
            {
                 $this->alerte('warning', 'Les mots de passe doivent être identiques.');
                 $this->genererVue();        
            }
            elseif(!filter_var($this->requete->getParametre("mail"), FILTER_VALIDATE_EMAIL))
            {
                $this->alerte('warning', 'Le champ e-mail n\'est pas valide.');
                $this->genererVue();
            }
            elseif($this->utilisateur->pseudoUtilise($this->requete->getParametre("pseudo")))
            {
                $this->alerte('warning', 'Pseudo déjà utilisé, merci d\'en choisir un autre');
                $this->genererVue();
            }
            elseif($this->utilisateur->mailUtilise($this->requete->getParametre("mail")))
            {
                $this->alerte('warning', 'E-mail déjà utilisée, merci d\'en choisir une autre');
                $this->genererVue();
            }
            elseif(!$this->requete->existeParametre('g-recaptcha-response') || !$this->captchaOk($this->requete->getParametre('g-recaptcha-response')))
            {
                $this->alerte('error', 'Ce site n\'est pas fait pour les robots, enfin !');
                $this->genererVue();
            }
            else
            {
                $prenom = ($this->requete->existeParametre("prenom")) ? $this->requete->getParametre("prenom") : "";
                $nom = ($this->requete->existeParametre("nom")) ? $this->requete->getParametre("nom") : "";
                $this->utilisateur->insertUtilisateur($this->requete->getParametre("pseudo"), $this->requete->getParametre("mail"), sha1($this->requete->getParametre("password")), $prenom, $nom);
                $this->alerte('success', 'Vous êtes maintenant inscrit ! Vous pouvez vous connecter.');
                $this->rediriger('auth','index');

            }
        }
        else
        {
            $this->genererVue();
        }        
    }

    private function captchaOk($code, $ip = null)
    {
        if (empty($code)) {
            return false; // Si aucun code n'est entré, on ne cherche pas plus loin
        }
        $params = [
            'secret'    => '6LecMyIUAAAAAIsx6ycq5zPin4rPedTqY6YtT54h',
            'response'  => $code
        ];
        if( $ip ){
            $params['remoteip'] = $ip;
        }
        $url = "https://www.google.com/recaptcha/api/siteverify?" . http_build_query($params);
        if (function_exists('curl_version')) {
            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_HEADER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_TIMEOUT, 1);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false); // Evite les problèmes, si le ser
            $response = curl_exec($curl);
        } else {
          // Si curl n'est pas dispo, un bon vieux file_get_contents
            $response = file_get_contents($url);
        }

        if (empty($response) || is_null($response)) {
            return false;
        }

        $json = json_decode($response);
        return $json->success;
    }

    public function profile()
    {
        if(!$this->requete->getSession()->existeAttribut("idUtilisateur"))
        {
            throw new Exception("Page web inaccessible");
        }
        $util = $this->utilisateur->getUtilisateur($this->requete->getSession()->getAttribut("login"));
       
        if($this->requete->existeParametre("envoi"))
        {
            $mdp = ($this->requete->existeParametre("password")) ? $this->requete->getParametre("password") : "";
            $confirm = ($this->requete->existeParametre("confirm")) ? $this->requete->getParametre("confirm") : "";
            if(!$this->requete->existeParametre("pseudo"))
            {
                $this->alerte('warning', 'Le champ "pseudo" est obligatoire.'); 
                $this->genererVue();         
            }
            elseif(!$this->requete->existeParametre("mail"))
            {
                 $this->alerte('warning', 'Le champ "Mail" est obligatoire.');  
                $this->genererVue();

            }
            elseif(strlen($this->requete->getParametre("pseudo")) < 5)
            {
                 $this->alerte('warning', 'Votre pseudo est trop court.');    
                $this->genererVue();                   
            }
            elseif(!empty($mdp) && $mdp != $confirm)
            {
                 $this->alerte('warning', 'Les mots de passe doivent être identiques.');
                 $this->genererVue();        
            }
            elseif(!filter_var($this->requete->getParametre("mail"), FILTER_VALIDATE_EMAIL))
            {
                $this->alerte('warning', 'Le champ e-mail n\'est pas valide.');
                $this->genererVue();
            }
            elseif(strcmp($this->requete->getParametre("pseudo"),$this->requete->getSession()->getAttribut("login")) && $this->utilisateur->pseudoUtilise($this->requete->getParametre("pseudo")))
            {
                $this->alerte('warning', 'Pseudo déjà utilisé, merci d\'en choisir un autre');
                $this->genererVue();
            }
            elseif(strcmp($this->requete->getParametre("mail"),$this->requete->getSession()->getAttribut("mail")) && $this->utilisateur->mailUtilise($this->requete->getParametre("mail")))
            {
                $this->alerte('warning', 'E-mail déjà utilisée, merci d\'en choisir une autre');
                $this->genererVue();
            }
            else
            {
                if(!$this->requete->existeParametre('avatar')) $avatar = ''; else $avatar = $this->requete->getParametre('avatar');
                $this->utilisateur->editUtilisateur($this->requete->getSession()->getAttribut("idUtilisateur"), $this->requete->getParametre("pseudo"), $this->requete->getParametre("mail"), $this->requete->getParametre("prenom"), $this->requete->getParametre("nom"), $avatar,sha1($mdp));
                $this->alerte('success', 'Profil modifié');
                $this->rediriger('auth','profile');

            }
        }
        else
        {
            $this->genererVue(array("util" => $util));
        }
        
    }
}