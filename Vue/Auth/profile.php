<?php $this->titre = "Modifier mon profil"; ?>

<form action="#" method="POST">
<div class="row uniform">
	<div class="4u 12u$(xsmall)">
		<label for="pseudo">Pseudo :</label>
	</div>
	<div class="8u 12u$(xsmall)">
		<input type="text" name="pseudo" id="pseudo" value="<?= $util['login']; ?>" disabled />
	</div>

	<div class="4u 12u$(xsmall)">
		<label for="pseudo">Nom :</label>
	</div>
	<div class="4u 12u$(xsmall)">
		<input type="text" name="nom" id="nom" placeholder="Nom (facultatif)" value="<?= $util['nom']; ?>"/>
	</div>
	<div class="4u 12u$(xsmall)">
		<input type="text" name="prenom" id="prenom" placeholder="Prénom (facultatif)" value="<?= $util['prenom']; ?>"/>
	</div>
	
	<div class="4u 12u$(xsmall)">
		<label for="mail">E-mail :</label>
	</div>

	<div class="8u 12u$(xsmall)">
		<input type="email" name="mail" id="mail" value="<?= $util['mail']; ?>"/>
	</div>

	<div class="4u 12u$(xsmall)">
		<label for="password">Mot de passe :</label>
	</div>
	<div class="8u 12u$(xsmall)">
		<input type="password" name="password" id="password" placeholder="Laisser vide pour ne pas le modifier" />
	</div>
	<div class="4u 12u$(xsmall)">
		<label for="confirm">Confirmez votre mot de passe :</label>
	</div>
	<div class="8u 12u$(xsmall)">
		<input type="password" name="confirm" id="confirm" placeholder="Laisser vide pour ne pas le modifier" />
	</div>
	<div class="4u 12u$(xsmall)">
		<label for="avatar">Avatar :</label>
	</div>
	<div class="8u 12u$(xsmall)">
		<input type="text" name="avatar" id="avatar" placeholder="Lien (url) vers votre avatar" value="<?= $util['avatar']; ?>"/>
	</div>
	<div class="actions">
		<input type="submit" name="envoi" value="Modifier" />
	</div>
</div>
</form>