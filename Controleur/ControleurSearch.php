<?php

require_once 'Framework/Controleur.php';
require_once 'Modele/Utilisateur.php';
require_once 'Modele/Biere.php';

class ControleurSearch extends Controleur {

    private $biere;
    private $utilisateur;

    public function __construct() {
        $this->biere = new Biere();
        $this->utilisateur = new Utilisateur();
    }

    // Affiche la liste de tous les billets du blog
    public function index() {
        if($this->requete->existeParametre('envoi'))
        {
        	$bieres = $this->biere->rechercher($this->requete->getParametre('search'));
        	$users = $this->utilisateur->rechercher($this->requete->getParametre('search'));
     		$this->genererVue(array("bieres" => $bieres, "users" => $users));
        }
        else
        {
        	$this->genererVue();
        }
    }

}

