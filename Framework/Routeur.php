<?php

require_once 'Controleur.php';
require_once 'Requete.php';
require_once 'Vue.php';


class Routeur {

    
    public function routerRequete() {
        try {
            // Fusion des paramètres GET et POST de la requête
            // Permet de gérer uniformément ces deux types de requête HTTP
            $requete = new Requete(array_merge($_GET, $_POST));

            $parametres = parse_ini_file("Config/routeur.ini");
            if($requete->existeParametre('controleur') && $requete->existeParametre('action') && ($requete->existeParametre('id')))
            {
                $cont = $requete->getParametre('controleur');
                $acti = $requete->getParametre('action');
                $idUrl = $requete->getParametre('id');
                if(isset($parametres[$cont.'/'.$acti. '/'.$idUrl]))
                {
                    $redirection = explode(' ', $parametres[$cont.'/'.$acti. '/'.$idUrl]);
                    $requete->setParametre('controleur', (empty($redirection[0])) ? '' : $redirection[0]);
                    $requete->setParametre('action', (empty($redirection[1])) ? '' : $redirection[1]);
                    $requete->setParametre('id', (empty($redirection[2])) ? '' : $redirection[2]);
                }
                if(isset($parametres[$cont.'/'.$acti. '/*']))
                {
                    $redirection = explode(' ', $parametres[$cont.'/'.$acti. '/*']);
                    $requete->setParametre('controleur', (empty($redirection[0])) ? '' : $redirection[0]);
                    $requete->setParametre('action', (empty($redirection[1])) ? '' : $redirection[1]);
                }
            }else if($requete->existeParametre('controleur') && $requete->existeParametre('action'))
            {
                $cont = $requete->getParametre('controleur');
                $acti = $requete->getParametre('action');
                if(isset($parametres[$cont.'/'.$acti]))
                {
                    $redirection = explode(' ', $parametres[$cont.'/'.$acti]);
                    $requete->setParametre('controleur', (empty($redirection[0])) ? '' : $redirection[0]);
                    $requete->setParametre('action', (empty($redirection[1])) ? '' : $redirection[1]);
                    $requete->setParametre('id', (empty($redirection[2])) ? '' : $redirection[2]);
                }
                if(isset($parametres[$cont.'/*']))
                {
                    $redirection = explode(' ', $parametres[$cont.'/*']);
                    $requete->setParametre('controleur', (empty($redirection[0])) ? '' : $redirection[0]);
                }
            }else if($requete->existeParametre('controleur') && $requete->existeParametre('id'))
            {
                $cont = $requete->getParametre('controleur');
                $id = $requete->getParametre('id');
                if(isset($parametres[$cont.'/'.$id]))
                {
                    $redirection = explode(' ', $parametres[$cont.'/'.$id]);
                    $requete->setParametre('controleur', (empty($redirection[0])) ? '' : $redirection[0]);
                    $requete->setParametre('action', (empty($redirection[1])) ? '' : $redirection[1]);
                    $requete->setParametre('id', (empty($redirection[2])) ? '' : $redirection[2]);
                }
                if(isset($parametres[$cont.'/*']))
                {
                    $redirection = explode(' ', $parametres[$cont.'/*']);
                    $requete->setParametre('controleur', (empty($redirection[0])) ? '' : $redirection[0]);
                    $requete->setParametre('action', (empty($redirection[1])) ? '' : $redirection[1]);
                }
            }else if($requete->existeParametre('controleur'))
            {
                $cont = $requete->getParametre('controleur');
                if(isset($parametres[$cont]))
                {
                    $redirection = explode(' ', $parametres[$cont]);
                    $requete->setParametre('controleur', (empty($redirection[0])) ? '' : $redirection[0]);
                    $requete->setParametre('action', (empty($redirection[1])) ? '' : $redirection[1]);
                    $requete->setParametre('id', (empty($redirection[2])) ? '' : $redirection[2]);
                }
            }
            $controleur = $this->creerControleur($requete);
            $action = $this->creerAction($requete);

            $controleur->executerAction($action);
        }
        catch (Exception $e) {
            $this->gererErreur($e);
        }
    }

    
    private function creerControleur(Requete $requete) {
        // Grâce à la redirection, toutes les URL entrantes sont du type :
        // index.php?controleur=XXX&action=YYY&id=ZZZ

        $controleur = "Accueil";  // Contrôleur par défaut

        if ($requete->existeParametre('controleur')) {
            $controleur = $requete->getParametre('controleur');
            // Première lettre en majuscules
            $controleur = ucfirst(strtolower($controleur));
        }
        // Création du nom du fichier du contrôleur
        // La convention de nommage des fichiers controleurs est : Controleur/Controleur<$controleur>.php
        $classeControleur = "Controleur" . $controleur;
        $fichierControleur = "Controleur/" . $classeControleur . ".php";
        if (file_exists($fichierControleur)) {
            // Instanciation du contrôleur adapté à la requête
            require($fichierControleur);
            $controleur = new $classeControleur();
            $controleur->setRequete($requete);
            return $controleur;
        }
        else {
            throw new Exception("Fichier '$fichierControleur' introuvable, pas de redirection pour '$controleur' de trouvée");
        }
    }

    
    private function creerAction(Requete $requete) {
        $action = "index";  // Action par défaut
        if ($requete->existeParametre('action')) {
            $action = $requete->getParametre('action');
        }
        return $action;
    }

    
    private function gererErreur(Exception $exception) {
        $vue = new Vue('erreur');
        $vue->generer(array('titre' => "Page web inaccessible", 'msgErreur' => $exception->getMessage()));
    }

}
