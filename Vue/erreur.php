<?php $this->titre = "Page inaccessible"; ?>

			<section id="three" class="wrapper">
				<div class="inner">
					<header class="align-center">
						<h2>Houston, on a un problème !</h2>
						<p>Cette page rencontre une erreur... Si ce problème persiste, contactez un webmaster !</p>
						<pre><code><?= $this->nettoyer($msgErreur) ?></code></pre>
					</header>
				</div>
			</section>
