<?php $this->title = "Rechercher"; ?>
<form action="#" method="POST">
	<div class="row uniform">
			<div class="8u 12u$(xsmall)">
				<input type="text" name="search" placeholder="Recherche une personne, une biere, ..." />
			</div>
			<div class="4u 12u$(xsmall)">
				<input type="submit" name="envoi" value="Rechercher" />
			</div>
	</div>
</form>

<div>
<?php
if(!empty($bieres))
{
 foreach($bieres as $biere){ ?>
<blockquote>
	<h4><a href="<?= $this->lien('biere/show/'.$biere->id); ?>"><?= $biere->nom; ?> <em>(Bière)</em></a></h4>
</blockquote>
<?php } } ?>
<?php if(!empty($users)) { foreach($users as $user){ ?>
<blockquote>
	<h4><a href="<?= $this->lien('user/'.$user->UTIL_ID); ?>"><?= $user->UTIL_LOGIN; ?> <em>(Utilisateur)</em></a></h4>
</blockquote>
<?php } }?>
</div>