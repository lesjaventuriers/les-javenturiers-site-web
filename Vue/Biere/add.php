<?php $this->titre = "Création d'une nouvelle bière";
require_once 'Framework/Modele.php'; ?>
<form action="#" method="POST">
<div class="row uniform">
	<div class="4u 12u$(xsmall)">
		<label for="nom">Nom :</label>
	</div>
	<div class="8u 12u$(xsmall)">
		<input type="text" name="nom" id="nom" required/>
	</div>
	
	<div class="4u 12u$(xsmall)">
		<label for="img">Source de l'image :</label>
	</div>
	<div class="8u 12u$(xsmall)">
		<input type="text" name="img" id="img"/>
	</div>

	<div class="4u 12u$(xsmall)">
		<label for="type">Type de bière :</label>
	</div>

	<div class="8u 12u$(xsmall)">
		<select name="type" id="type" required>
			<option value="blonde"> Blonde </option>
			<option value="blonde speciale"> Blonde spéciale </option>
			<option value="ambree"> Ambrée </option>
			<option value="brune"> Brune </option>
			<option value="blanche"> Blanche </option>
		</select>
	</div>

	<div class="4u 12u$(xsmall)">
		<label for="degre">Degré d'alcool :</label>
	</div>
	<div class="8u 12u$(xsmall)">
		<input type="number" name="degre" id="degre" step="0.1" min="0" max="100"/>
	</div>

	<div class="4u 12u$(xsmall)">
		<label for="brasseur">Brasseur :</label>
	</div>
	<div class="8u 12u$(xsmall)">
		<input type="text" name="brasseur" id="brasseur" />
	</div>
	<div class="actions">
		<input type="submit" name="envoi" value="Nouv biere" onsubmit="verif_data()" />
	</div>
</div>
</form>