<?php $this->titre = "A propos"; ?>
<header class="align-center">
						<h2>Les Jav'enturiers</h2>
						<p>Apportez votre bière à l'édifice !</p>
					</header>
					<p>Nous sommes comme notre nom l'indique les Jav'enturiers. Venue du fond de la Lorraine, notre équipe est 
					composée <b>uniquement</b> de 1ères années. Eh oui, il faut oser.</p>
					<p></p>
					<div class="flex flex-2">
						<article>
							<div class="image fit">
								<img src="Contenu/images/img_1.jpg" alt="Pic 01" />
							</div>
							<header>
								<h3>Nos fonctionnalités...</h3>
							</header>
							<p>
								<li> Ajouter vos bières préférées et contribuer </li>
								<li> Attribuer une note et donner votre avis dessus</li>
								<li> Consulter et rechercher un nombre incalculable de bières !</li>
								<li> Trouver des compagnons de beuverie (ou plus si affinité...) ayant les mêmes goûts que vous </li>
				
							</p>

						</article>
						 <article> 
							<div class="image fit">
								<img src="Contenu/images/nous.jpg" alt="Pic 02" />
							</div> 
							<header>
								<h3>Les Jav'enturiers au complet et (presque) en pleine forme </h3>
							</header>
							<p> Face aux amateurs de bières de plus en plus nombreux, une seule équipe a eu cette idée géniale : 
							Leur donner la possibilité de se rencontrer !! </p>
						</article> 