<?php

require_once 'Framework/Controleur.php';
require_once 'Modele/Biere.php';


class ControleurBiere extends Controleur
{
    private $biere;

    public function __construct()
    {
        $this->biere = new Biere();
    }

    public function index() {

    }

    public function add() {
         if(!$this->requete->getSession()->existeAttribut("idUtilisateur"))
        {
            $this->alerte('error', 'Vous devez être connectés.');
            $this->rediriger("accueil");
        }

        if($this->requete->existeParametre("envoi"))
        {
            if(!$this->requete->existeParametre("nom"))
            {
                $this->alerte('warning', 'Le champ "Nom" est obligatoire.'); 
                $this->genererVue();         
            }
            elseif(!$this->requete->existeParametre("type"))
            {
                 $this->alerte('warning', 'Le champ "Type de biere" est obligatoire.');     
                $this->genererVue();

            }
            elseif($this->biere->existeDeja($this->requete->getParametre("nom")))
            {
                $this->alerte('warning', 'Nom de biere déjà enregistré');
                $this->genererVue();
            }
            else
            {
                $brasseur = ($this->requete->existeParametre('brasseur')) ? $this->requete->getParametre('brasseur') : "";
                $img = ($this->requete->existeParametre('img')) ? $this->requete->getParametre('img') : "";
                $degre = ($this->requete->existeParametre('degre')) ? $this->requete->getParametre('degre') : "";
                $this->biere->insertBiere($this->requete->getParametre("nom"), $this->requete->getParametre("type"), $degre, $brasseur,$img);
                $this->alerte('success', 'Biere ajoutée. Merci pour votre participation.');
                $biereLast = $this->biere->getId($this->requete->getParametre("nom"));
                $this->rediriger('biere','show/'.$biereLast->id);

            }
        }
        else
        {
            $this->genererVue();
        }   
    }
	
public function show(){
		$id=$this->requete->getParametre("id");
		$biere=$this->biere->recuperer($id);
        $moy = $this->biere->moyenne($id);
		$this->genererVue(array("biere"=>$biere,'moy'=>$moy));

        if($this->requete->existeParametre("note") && $this->requete->existeParametre("message")) {
            $note = $this->requete->getParametre("note") ;
            $idutil =  $this->requete->getSession()->getAttribut("idUtilisateur") ;
            $idbiere = $this->requete->getParametre("id") ;
            $com = $this->requete->getParametre("message");
            $this->biere->insererCom($note,$idutil, $idbiere, $com) ;
            $this->alerte('success', 'Commentaire ajouté !');
            $this->rediriger('biere', 'show/'.$idbiere);
        }
	}
}

/*public function modify(){
    if($this->requete->existeParametre("brasseur")) {
            $brasseur= $this->requete->getParametre("brasseur") ;
        }

     if($this->requete->existeParametre("img")) {
            $url=$this->requete->getParametre("img") ;
        }

    if($this->requete->existeParametre("degre")) {
        $bonType=false ;
        if(is_int($_POST("degre"))) {
            if($this->requete->getParametre("degre")>=0 && $this->requete->getParametre("degre")<=100) {
                $degre = $this->requete->getParametre("degre");
}
}
*/


