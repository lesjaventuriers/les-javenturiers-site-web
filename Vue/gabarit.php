<?php $nomSite = "Beer-to-beer"; ?>
<!DOCTYPE HTML>
<html>
    <head>
        <title><?= $titre." - ".$nomSite; ?></title>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="stylesheet" href="<?= $this->lien('Contenu/css/main.css'); ?>" />
    </head>
    <body class="subpage">

        <!-- Header -->
            <header id="header">
                <div class="inner">
                    <a href="<?= $this->lien('accueil'); ?>" class="logo"><?= $nomSite; ?></a>
                    <nav id="nav">
                        <a href="<?= $this->lien('accueil'); ?>">Accueil</a>
                        <?php if(isset($session))
                             if($session->existeAttribut('idUtilisateur'))
                        {  ?>
                            <a href="<?= $this->lien('biere/add'); ?>">Ajouter une bière </a>
                            <a class="recherche" href="<?= $this->lien('search'); ?>">Recherche </a>
                            <a href="<?= $this->lien('user/'.$session->getAttribut('idUtilisateur')); ?>">Profil </a>
                            <a href="<?= $this->lien('auth/logout'); ?>">Se déconnecter </a>
                        <?php }else{ ?>
                            <a href="<?= $this->lien('auth'); ?>">Connexion</a>
                            <a href="<?= $this->lien('auth/register'); ?>">S'inscrire</a>
                            
                            <?php } ?>
                             
                            <a href="<?= $this->lien('infos'); ?>">A propos</a>
                    </nav>
                    <a href="#navPanel" class="navPanelToggle"><span class="fa fa-bars"></span></a>
                </div>
            </header>

            <?php
            if(isset($session)){

            if($session->existeAttribut("msgalertsyserror"))
            {
                
                echo "<div class=\"alert alert-error\">".$session->getAttribut("msgalertsyserror")."</div>";
                $session->deleteAttribut("msgalertsyserror");
            }

            if($session->existeAttribut("msgalertsyssuccess"))
            {
                echo "<div class=\"alert alert-success\">".$session->getAttribut("msgalertsyssuccess")."</div>";
                $session->deleteAttribut("msgalertsyssuccess");
            }

            if($session->existeAttribut("msgalertsyswarning"))
            {
                echo "<div class=\"alert alert-warning\">".$session->getAttribut("msgalertsyswarning")."</div>";
                $session->deleteAttribut("msgalertsyswarning");
            }

            if($session->existeAttribut("msgalertsysinfo"))
            {
                echo "<div class=\"alert alert-info\">".$session->getAttribut("msgalertsysinfo")."</div>";
                $session->deleteAttribut("msgalertsysinfo");
            }

            }
            ?>
            <?php if(strcmp($this->titre,"Accueil"))
            { ?>
            <section id="three" class="wrapper">
                <div class="inner">
            <?php } ?>
                <?= $contenu ?>

            <?php if(strcmp($this->titre,"Accueil"))
            { ?>
                </div>
            </section>
            <?php } ?>
            <!-- Footer -->
            <footer id="footer">
                <div class="inner">
                    <div class="flex">
                        <div class="copyright">
                            &copy; <?= $nomSite; ?>. Réalisé par Les Jav'enturiers lors des 24h des IUT 2017
                        </div>
                    </div>
                </div>
            </footer>

        <!-- Scripts -->
            <script src="<?= $this->lien('Contenu/js/jquery.min.js'); ?>"></script>
            <script src="<?= $this->lien('Contenu/js/skel.min.js'); ?>"></script>
            <script src="<?= $this->lien('Contenu/js/util.js'); ?>"></script>
            <script src="<?= $this->lien('Contenu/js/main.js'); ?>"></script>

    </body>
</html>